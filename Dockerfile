# hxe-min
#
# Deloitte Digital Deutschland
# cdeba@deloitte.de

FROM scriptico/rhel7-hxe-ready:1.0

ARG HOSTNAME
ARG SID
ARG INSTANCE_NUMBER
ARG MASTER_PASSWORD

WORKDIR /tmp

#
# Installing SAP HANA Express 2.0
#
COPY distributions/hxe.tgz hxe.tgz

RUN tar -xzf hxe.tgz; rm -rf /tmp/hxe.tgz; { echo '/tmp/HANA_EXPRESS_20'; echo "$HOSTNAME"; echo "$SID"; echo "$INSTANCE_NUMBER"; echo "$MASTER_PASSWORD"; echo "$MASTER_PASSWORD"; echo 'Y'; } | ./setup_hxe.sh; rm -rf /tmp/HANA_EXPRESS_20; rm -f ./setup_hxe.sh; exit 0