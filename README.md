# SAP HANA Express on Docker #

This is an utility to configure and build a Docker image with SAP HANA Express 2.0 (server+AFL only) installed


### Requirements ###

* Docker 1.13.0 (stable channel)
* 16 GB of Memory

### Configure and Build Image ###

* Pull the most recent version of the helper from *git@bitbucket.org:cdeba/docker-hxe.git*.
* Download the SAP HANA Express 2.0 server only binary installer as described in SAP Tutorial Catalog.
* Copy hxe.tgz into docker-hxe/distributions. Do not unpack the archive.
* Run docker-hxe/build-image.sh from Terminal


### Image Details ###
* System Usage: development
* Location of Data Volumes: /hana/shared/data/HXE
* Location of Log Volumes: /hana/shared/log/HXE
* System Administrator Home Directory: /usr/sap/HXE/home

### Essential Commands ###

#### Run a container. ####
```
docker run -i -t --privileged -p 4390:4390 -p 8090:8090 -p 39013:39013 -p 39015:39015 -p 39018:39018 -p 59013:59013 -p 59014:59014 --hostname=hxe-dd --name=hxe-min hxe-min:latest /bin/bash
```
#### Start the container. ####
```
docker start hxe-min
```
#### Attach to the started container. ####
```
docker attach hxe-min
```
#### Save changes in the container as a new image. ####
```
docker commit --author="Your Name" -m "Changes have been made" -p hxe-min image_name:image_tag
```
#### Delete the container. ####
```
docker rm -f hxe-min
```
#### See the container status. ####
```
docker ps -a
```
### Manage HANA in Container ###
The following commands must be performed under the hxeadm user.
```
su hxeadm
```
#### DB current status. ####
```
HDB info
```
#### Start / Stop / Restart HANA. ####
```
HDB start [stop | restart]
```