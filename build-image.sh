#!/bin/bash
#
# Deloitte Digital Deutschland
# cdeba@deloitte.de


# You may override the following properties

IMAGE_NAME="hxe-min"
IMAGE_TAG="latest"

HOSTNAME="hxe-min"
SID="HXE"
INSTANCE_NUMBER=90
MASTER_PASSWORD="Ma3terPassw0rd"

echo "The script will build a Docker image with SAP HANA Express 2.0 (DB only) installed."
echo ""
echo "The image will be named as $IMAGE_NAME and tagged with the $IMAGE_TAG tag."
echo ""
echo "The following DB details will be in use:"

echo -e "\tHostname: $HOSTNAME"
echo -e "\tSID: $SID"
echo -e "\tInstance Number: $INSTANCE_NUMBER"
echo -e "\tMaster Password: $MASTER_PASSWORD"
echo ""
echo "If you need to change any of the properties above, please answer NO on the following question"
echo "and adjust the properties in build-image.sh"
echo ""
echo "Would you like to continue? [Y]es / [N]o"
while read start_process; do
    case "$start_process" in
        y)  echo "The build process has been started."
            break
            ;;
        n)  exit 0
            ;; 
        *) echo "press either y to continue or n to exit"
            continue
           ;;
    esac
done


START=$(date +%s)

docker build . --build-arg HOSTNAME=$HOSTNAME --build-arg SID=$SID --build-arg INSTANCE_NUMBER=$INSTANCE_NUMBER --build-arg MASTER_PASSWORD=$MASTER_PASSWORD --no-cache=true --squash=true  -t="$IMAGE_NAME:$IMAGE_TAG"

END=$(date +%s)

echo "y" | docker system prune

docker images

echo ""
echo "It took $(( $END - $START )) seconds to build the $IMAGE_NAME:$IMAGE_TAG image."
echo "Goodbye!"